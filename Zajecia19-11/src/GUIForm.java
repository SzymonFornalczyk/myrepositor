import javax.swing.*;

public class GUIForm {
    private JButton button1;
    private JButton button2;
    private JTextArea textArea1;
    private JPanel panel1;

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUIForm");
        frame.setContentPane(new GUIForm().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
