import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ImageFrame extends JFrame implements ActionListener {

    public ImageFrame() {
        super( "Słoooońce" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(50,50);
        setLayout(new FlowLayout());

        ImagePanel img = new ImagePanel("slonce.png");
        add(img);

        pack();
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ImageFrame();
            }
        });
    }
}
