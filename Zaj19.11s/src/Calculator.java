import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements ActionListener {

    JButton bAction1,bAction2,bAction3,bAction4;
    JTextField textArea1, textArea2;
    JLabel label1,label2,label3;

    public Calculator() {
        super( "Hello World" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 600);
        setLocation(50,50);
        setLayout(new FlowLayout());

        bAction1 = new JButton("+");
        bAction1.addActionListener(this);
        bAction2 = new JButton("-");
        bAction2.addActionListener(this);
        bAction3 = new JButton("+");
        bAction3.addActionListener(this);
        bAction4 = new JButton("/");
        bAction4.addActionListener(this);


        textArea1 = new JTextField();
        textArea1.setPreferredSize(new Dimension(50,20));
        textArea2 = new JTextField();
        textArea2.setPreferredSize(new Dimension(50,20));

        label1 = new JLabel("Czynnik 1");
        label2 = new JLabel("Czynnik 2");
        label3 = new JLabel("Wynik");

        add(bAction1);
        add(bAction2);
        add(bAction3);
        add(bAction4);
        add(textArea1);
        add(label1);
        add(textArea2);
        add(label2);
        add(label3);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(bAction1)) {
            label1.setText("przycisk 1");
            //System.out.println("wprowadzony tekst: " + textArea.getText());
        }
        if (e.getSource().equals(bAction2)) {
            label2.setText("przycisk 2");
        }
        if (e.getSource().equals(textArea1)) {
            label3.setText(textArea1.getText());
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override

            public void run() {
                new Calculator();
            }
        });
    }
}