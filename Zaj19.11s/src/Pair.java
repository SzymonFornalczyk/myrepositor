public class Pair<K, V> {

    private K key;
    private V value;


    public Pair() {
        key = null;
        value = null;
    }

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public Pair(Pair<K, V> pair) {
        this.key = pair.key;
        this.value = pair.value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public String toString() {
        return "(" + key + "->" + value + ")";
    }


    public boolean equals(Pair<K, V> pair) {
        return (key.equals(pair.key) && value.equals(pair.value));
    }

    public static void main(String[] args) {
        Pair<Integer,Integer> pint = new Pair<>(8,12);
        Pair<Integer,String> owoc = new Pair<>(5,"jabłko");
        Pair<String,String> gdzieOwoc = new Pair<>("lodówka","jabłko");
        Pair<String,String> infokopia = new Pair<>(gdzieOwoc);
        gdzieOwoc.setKey("szafce");


        System.out.println(pint);
        System.out.println(owoc);
        System.out.println(gdzieOwoc);
        System.out.println(infokopia);

        System.out.println("Gdzie są moje owoce?");
        System.out.println(gdzieOwoc.getValue() + " znajduje sie w " + gdzieOwoc.getKey());

        Pair<String,Pair<Integer,String>> extpair = new Pair<>("To jest trudne", new Pair<>(5,"czesc"));
        System.out.println(extpair);



    }

}


