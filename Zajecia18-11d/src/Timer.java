public class Timer implements Runnable {

    private boolean finalize = false;

    public void finish() {
        finalize = true;
    }

    public void run() {
        int time = 0;
        while (! finalize) {
            try {
                Thread.sleep(1000);
            } catch(InterruptedException exc) {
                System.out.println("Wątek został przerwany.");
                return;
            }
            time++;
            int minutes = time/60;
            int sec = time%60;
            System.out.println(minutes + ":" + sec);
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Timer t = new Timer();
        Thread th = new Thread(t);
        th.start();

        Thread.sleep(500);

        Timer t2 = new Timer();
        Thread th2 = new Thread(t2);
        th2.start();

        Thread.sleep(10000);
        t.finish();
        Thread.sleep(4000);
        t2.finish();
    }

}
