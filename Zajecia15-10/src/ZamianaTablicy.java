import java.util.Random;

public class ZamianaTablicy {

    public static void main(String[] args) {

        Random gen = new Random();
        int[] tablica = new int[10];

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = gen.nextInt(20);
            System.out.print("" + tablica[i] + ", ");
        }
        System.out.println();

        for (int i = 0; i < tablica.length / 2; i++) {
            int temp;
            int skad = i;
            int dokad = tablica.length - i - 1;

            temp = tablica[skad];
            tablica[skad] = tablica[dokad];
            tablica[dokad] = temp;
        }

        for (int i = 0; i < tablica.length; i++) {
            System.out.print("" + tablica[i] + ", ");
        }

    }

}
