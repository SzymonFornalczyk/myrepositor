import java.util.Random;
import java.util.Scanner;

public class Losowanie4 {

    public static void main(String[] args) {

        int pocz_przedzialu, koniec_przedzialu;
        double[] tablica = new double[10];

        Scanner cin = new Scanner(System.in);
        Random gen = new Random();

        System.out.println("Wprowadź początek przedziału: ");
        pocz_przedzialu = cin.nextInt();
        System.out.println("Wprowadź koniec przedziału: ");
        koniec_przedzialu = cin.nextInt();

        int ilosc_liczb = koniec_przedzialu - pocz_przedzialu;

        for (int i = 0; i < 10; i++) {
            tablica[i] = (double)(gen.nextInt(ilosc_liczb) + pocz_przedzialu);
            tablica[i] = tablica[i] + gen.nextDouble();
        }

        for (int i = 0; i < 10; i++) {
            System.out.println("tablica[" + i + "] = " + tablica[i]);
        }
    }

}
