import java.util.Scanner;

public class ZamianaElementow {

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);

        int liczba_A, liczba_B;

        System.out.print("Wprowadź liczbę A: ");
        liczba_A = cin.nextInt();
        System.out.print("Wprowadź liczbę B: ");
        liczba_B = cin.nextInt();

        System.out.println();
        System.out.println("Zamieniam...");
        System.out.println();

        int temp;
        temp = liczba_A;
        liczba_A = liczba_B;
        liczba_B = temp;

        System.out.println("Liczba A: " + liczba_A);
        System.out.println("Liczba B: " + liczba_B);

    }

}
