import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Random;
import java.util.Scanner;

public class Zad7_2 {
    public static void losuj(int[][] T) {
        Random r = new Random();

        for (int i = 0; i < T.length; i++) {
            for (int j = 0; j < T[i].length; j++) {
                T[i][j] = r.nextInt(10);
            }
        }
    }

    public static void wypisz(int[][] T) {
        for (int i = 0; i < T.length; i++) {
            System.out.print("[");
            for (int j = 0; j < T[i].length; j++) {
                System.out.print(T[i][j]);
                if (j < T[i].length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println("]");
        }
    }

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        System.out.println("Wprowadz dane do 1 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");

        int a = cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int b = cin.nextInt();
        int[][] macierz = new int[a][b];
        System.out.println();

        Zad7_2.losuj(macierz);
        Zad7_2.wypisz(macierz);

        System.out.println("Wprowadz dane do 2 tablicy.");
        System.out.println("Ile wierszy chcesz wprowadzic do tablicy?");
        int c = cin.nextInt();
        System.out.println("Ile kolumn chcesz wprowadzic do tablicy?");
        int d = cin.nextInt();
        int[][] macierz2 = new int[c][d];
        System.out.println();

        Zad7_2.losuj(macierz2);
        Zad7_2.wypisz(macierz2);

        System.out.println();
        int[][] macierz3 = new int[a][b];

        if (macierz.length != macierz2.length || macierz[0].length != macierz2[0].length) {
            System.out.println(" Macierze nie są równe");

        } else {
            for (int i = 0; i < macierz.length; i++) {
                for (int j = 0; j < macierz[i].length; j++) {
                    macierz3[i][j] = macierz2[i][j] + macierz[i][j];
                }

            }
            System.out.println("Powstała macierz 3");
            Zad7_2.wypisz(macierz3);
        }

        System.out.println();
         int macierz4 [][] = new int [a][b];

        if (macierz.length != macierz2.length || macierz[0].length != macierz2[0].length) {
            System.out.println(" Macierze nie są równe");

        }else {
            for (int i = 0; i <macierz.length ; i++) {
                for (int j = 0; j <macierz.length ; j++) {
                    macierz4[i][j]= macierz[i][j] * macierz2[i][j];

                }

            }
            System.out.println("Mnożenie macierzy");
            Zad7_2.wypisz(macierz4);
        }





    }


}


