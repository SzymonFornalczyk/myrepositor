import java.util.Scanner;

public class TablicaLiczb {

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        int n;
        int[] liczby;

        System.out.print("Ile chciałbyś liczb? ");
        n = cin.nextInt();

        liczby = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Wprowadź " + (i+1) + " liczbę: ");
            liczby[i] = cin.nextInt();
        }

        for (int i = 0; i < n; i++) {
            System.out.println("liczby[" + i + "] = " + liczby[i]);
        }
    }
}
