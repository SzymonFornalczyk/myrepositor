import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class EuropeCapitals {

    Map<String, String> data;

    public EuropeCapitals(String filename) {

        data = new HashMap<>();
        File plik = new File(filename);
        Scanner in;
        try {
            in = new Scanner(plik);
            while (in.hasNextLine()) {
                String linia = in.nextLine();
                linia = linia.replaceAll("\\[.\\]", "").trim();

                String[] temp = linia.split("\\|");
                data.put(temp[0], temp[1]);
            }
        } catch (Exception e) {
            System.out.println("Brak pliku...");

        }
    }

    public void printData() {
        for (Map.Entry<String, String> element : data.entrySet()) {
            System.out.println(element.getKey() + " -> " + element.getValue());
        }
    }

    public String getValue(String country) {
        return data.get(country);
    }

    public int getSize() {
        return data.size();
    }

    public String getRandomCountry() {
        Random r = new Random();
        int i = r.nextInt(data.size());     //wylosowana liczba
        int j = 0;                          //licznik przejrzanych

        //entrySet rzutuje mapę na zbiór, dzięki czemu możemy ją obejrzeć element po elemencie za pomocą pętli foreach
        for (Map.Entry<String, String> element : data.entrySet()) {
            if (i == j) {
                return element.getKey();
            }

            j++;
        }

        return null;
    }

    public boolean checkCapital(String country, String capital) {
        return getValue(country).equals(capital);
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        EuropeCapitals ec = new EuropeCapitals("capitals.txt");

        int points = 0;
        for (int i = 0; i < 10; i++) {
            String country = ec.getRandomCountry();
            System.out.print(country + "? ");
            String capital = cin.nextLine();

            if (ec.checkCapital(country, capital)) {
                points++;
            }

        }
        System.out.println("Zdobyłeś "+points +" punktów. Gratuluję!");
    }



}