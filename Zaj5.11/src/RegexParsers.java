import java.util.Scanner;

public class RegexParsers {
    public static boolean checkColor(String arg) {
        String regexColor = "red|green|blue|black|white";
        return arg.matches(regexColor) || arg.matches(regexColor.toUpperCase()) || arg.matches(regexColor.toLowerCase());
    }

    public static boolean checkDate(String arg) {
        String regexinteger = "[0-3]?[1-9]/[0-9]{2}[0-9]4";
        return arg.matches(regexinteger);
    }

    public static boolean checkInteger(String arg) {
        String regexInteger = "[-+]?[0-9]+";
        return arg.matches(regexInteger);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        String s;
        do {
            System.out.println("wprowadz kolor");
            s = cin.nextLine();
            if (checkColor(s)) {
                System.out.println("Poprawnie");
            } else {
                System.out.println("Niepoprawnie");
            }
        } while (!s.equals(""));
    }

}
