public class Pies implements Zwierze {
    double masa;

    @Override
    public String jedz() {
        return "Przeciez jem";
    }

    public Pies rozmnozSie() {
        Pies malypies = new Pies();
        malypies.masa = 300;
        return malypies;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o " + masa);
        this.masa +=masa;
    }
}