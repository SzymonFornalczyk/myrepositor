public class Kot implements Zwierze {
    double masa;

    @Override
    public String jedz() {
        return "Przeciez jem";
    }

    public Kot rozmnozSie() {
        Kot malykot = new Kot();
        malykot.masa = 200;
        return malykot;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o " + masa);
        this.masa += masa;
    }
}

