import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleCalculator {

    public static int suma(int a, int b) {
        return a + b;
    }

    public static int odejmowanie(int a, int b) {
        return a - b;
    }

    public static int mnozenie(int a, int b) {
        return a * b;
    }

    public static int divide(int a, int b) {
        return a / b;
    }

    public static double power(int a, int b) {
        return Math.pow(a, b);
    }


    public static void main(String[] args) {
        int a, b;
        Scanner cin = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("Wprowadź A: ");
                a = cin.nextInt();
                System.out.println("Wprowadź B: ");
                b = cin.nextInt();


                System.out.println("suma " + suma(a, b));
                System.out.println("odejmowanie " + odejmowanie(a, b));
                System.out.println("mnozenie " + mnozenie(a, b));
                System.out.println("potęga " + power(a, b));

            } catch (ArithmeticException e) {
                System.out.println("błąd obliczeń");
            } catch (InputMismatchException e) {
                System.out.println("błąd danych");
            }
            cin.nextLine();


        }
    }
}