public class Mysz implements Zwierze {
    double masa;

    @Override
    public String jedz() {
        return "Przeciez jem";
    }

    public Mysz rozmnozSie() {
        Mysz malamysz = new Mysz();
        malamysz.masa = 100;
        return malamysz;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o " + masa);
        this.masa += masa;
    }

    public void piszcz() {
        System.out.println("PIIIII, PII");
    }
}