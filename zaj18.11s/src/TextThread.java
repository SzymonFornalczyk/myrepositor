public class TextThread implements Runnable {

    public String tekst;
    public int mill;

    public TextThread(String tekst, int mill) {
        this.mill = mill;
        this.tekst = tekst;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(mill);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(tekst);
        }

    }
}