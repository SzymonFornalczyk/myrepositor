import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        double a, b;
        int c;
        System.out.println("Podaj pierwszą liczbę");
        a = cin.nextDouble();
        System.out.println("Podaj drugą liczbę");
        b = cin.nextDouble();

        Multiplication multi = new Multiplication();
        Exponentation exp = new Exponentation();

        System.out.println("Jakie działanie chcesz wykonać? 1- mnożenie 2- potęgowanie");
        c = cin.nextInt();
        switch (c) {
            case 1:
                System.out.println(multi.compute(a, b));
                break;
            case 2:
                System.out.println(exp.compute(a, b));
                break;
        }
    }
}
