public class Exponentation implements Computation {
    public double compute(double a, double b){
        return Math.pow(a,b);
    }
}
