public class SimpleTable {
    public static int suma(int... nums) {
        int wynik = 0;

        for (int i = 0; i < nums.length; i++) {
            wynik += nums[i];
        }
        return  wynik;
    }

    public static void czesc(String... imiona) {
        for (int i = 0; i < imiona.length ; i++) {
            System.out.println("cześć " + imiona[i]);

        }

    }


    public static void main(String[] args) {
        System.out.println(suma());
        System.out.println(suma(1,3,5,8,9));
        System.out.println(suma(2,4,5));

        System.out.println();
        czesc("łukasz","Szymon");
    }

}
