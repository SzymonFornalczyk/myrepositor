public class TestWarzywa {
    public static void main(String[] args) {
        Warzywo[] salatka = new Warzywo[5];
        salatka[0]= new Pomidor();
        salatka[1]= new Cebula();
        salatka[2]= new PomidorMalinowy();
        salatka[3]= new Pomidor();
        salatka[4]= new Cebula();

        for ( Warzywo warzywo : salatka){
            System.out.println(warzywo);
        }
    }
}
