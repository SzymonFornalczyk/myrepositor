public class Operatory {

    public static void main(String[] args) {

        int a = 4, b = 7;

        System.out.println("Dodawanie a + b: " + (a+b));
        System.out.println("Odejmowanie a - b: " + (a-b));
        System.out.println("Mnożenie a * b: " + (a*b));
        System.out.println("Dzielenie a / b: " + (a/b));
        System.out.println("Reszta a % b: " + (a%b));

        System.out.println("Porównanie a == b: " + (a==b));
        System.out.println("Porównanie a != b: " + (a!=b));
        System.out.println("Porównanie a < b: " + (a<b));
        System.out.println("Porównanie a <= b: " + (a<=b));
        System.out.println("Porównanie a > b: " + (a>b));
        System.out.println("Porównanie a >= b: " + (a>=b));

        System.out.println();

        a = -5; b = -5;

        System.out.println("Dodawanie a + b: " + (a+b));
        System.out.println("Odejmowanie a - b: " + (a-b));
        System.out.println("Mnożenie a * b: " + (a*b));
        System.out.println("Dzielenie a / b: " + (a/b));
        System.out.println("Reszta a % b: " + (a%b));

        System.out.println("Porównanie a == b: " + (a==b));
        System.out.println("Porównanie a != b: " + (a!=b));
        System.out.println("Porównanie a < b: " + (a<b));
        System.out.println("Porównanie a <= b: " + (a<=b));
        System.out.println("Porównanie a > b: " + (a>b));
        System.out.println("Porównanie a >= b: " + (a>=b));

        System.out.println();

        System.out.println("Dodawanie a + 8 - b: " + (a + 8 - b));
        System.out.println("Odejmowanie (a - b + 2)*2: " + ((a - b + 2)*2));

    }
}
