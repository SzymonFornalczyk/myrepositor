import javax.swing.*;

public class Calculator2 {

    public static void main(String[] args) {
        double liczba1, liczba2;
        String wejscie;

        wejscie = JOptionPane.showInputDialog("Hej. Wprowadź pierwszą liczbę:");
        wejscie = wejscie.replace(',', '.');    //podmieniam przecinki na kropki dla zgodności wprowadzania danych
        liczba1 = Double.parseDouble(wejscie);

        wejscie = JOptionPane.showInputDialog("Teraz wprowadź drugą liczbę:");
        wejscie = wejscie.replace(',', '.');
        liczba2 = Double.parseDouble(wejscie);

        JOptionPane.showMessageDialog(null,"" + liczba1 + " + " + liczba2 + " = " + (liczba1+liczba2));
        JOptionPane.showMessageDialog(null,"" + liczba1 + " - " + liczba2 + " = " + (liczba1-liczba2));
        JOptionPane.showMessageDialog(null,"" + liczba1 + " * " + liczba2 + " = " + (liczba1*liczba2));
        JOptionPane.showMessageDialog(null,"" + liczba1 + " / " + liczba2 + " = " + (liczba1/liczba2));
    }

}
