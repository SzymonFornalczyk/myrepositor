import java.util.Random;

public class Circle {

    private Point p;
    private double r;

    public Circle() {
        p = new Point();
        r = 0.0;
    }

    public Circle(int x, int y, double r) {
        p = new Point(x, y);
        this.r = r;
    }

    public Circle(Point p, double r) {
        this.p = new Point(p);
        this.r = r;
    }

    public Circle(Circle c) {
        p = new Point(c.p);
        r = c.r;
    }

    public Point getP() {
        return p;
    }

    public double getR() {
        return r;
    }

    public void setP(int x, int y) {
        p.set(x, y);
    }

    public void setR(double r) {
        this.r = r;
    }

    public String toString() {
        return "" + p + " -> " + r;
    }

    public boolean equals(Circle c) {
        return r == c.r;
    }

    public double getDiameter() {
        return 2.0 * r;
    }

    public double getArea() {
        return Math.PI * r * r;
    }

    public double getCircuit() {
        return 2.0 * Math.PI * r;
    }

    public boolean isInside(Point p2) {
        return p.distance(p2) <= r;
    }

    public boolean isInside(Point[] points) {
        for (Point a : points) {
            if (! isInside(a)) return false;
        }
//        for (int i = 0; i < points.length; i++) {
//            if (! isInside(points[i])) return false;
//        }

        return true;
    }

    public boolean isOverlapped(Circle c) {
        return p.distance(c.p) <= r + c.r;
    }


    //CZESC TESTUJACA:
    public static double round(double a, int d) {       //a - liczba do zaokrąglenia, d - ilość miejsc po przecinku
        double temp = Math.pow(10.0, (double)d);        //podnoszę 10 do potęgi równej liczbie miejsc po przecinky np. dla 3-ech uzyskam 1000
        a = a * temp;
        a = Math.round(a);
        a = a / temp;
        return a;
    }

    public static void main(String[] args) {
        Random r = new Random();

        Circle c1, c2, c3, c4;
        c1 = new Circle();
        c2 = new Circle(5, 3, 12.5);
        c3 = new Circle(new Point(4, 4), 5.0);
        c4 = new Circle(c2);

        c2.setP(-5, -5);
        c2.setR(4.2);

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c4);

        Point[] T = new Point[10];
        for (int i = 0; i < T.length; i++) {
            T[i] = new Point(r.nextInt(10), r.nextInt(10));
        }

        for (Point p : T) {
            System.out.print(p + ", ");
        }

        System.out.println();
        System.out.println("T w kolku c1: " + c1.isInside(T));
        System.out.println("T w kolku c2: " + c2.isInside(T));
        System.out.println("T w kolku c3: " + c3.isInside(T));
        System.out.println("T w kolku c4: " + c4.isInside(T));

        System.out.println("Pole c1: " + c1.getArea() + " " + round(c1.getArea(), 2));
        System.out.println("Pole c2: " + c2.getArea() + " " + round(c2.getArea(), 2));
        System.out.println("Pole c3: " + c3.getArea() + " " + round(c3.getArea(), 2));
        System.out.println("Pole c4: " + c4.getArea() + " " + round(c4.getArea(), 2));
        System.out.println("Obwód c1: " + c1.getCircuit());
        System.out.println("Obwód c2: " + c2.getCircuit());
        System.out.println("Obwód c3: " + c3.getCircuit());
        System.out.println("Obwód c4: " + c4.getCircuit());
    }
}
