public class Pracownik {
    String imie;
    String nazwisko;
    int wiek;

    public Pracownik(){
        this.imie= "nie podano imienia";
        this.nazwisko= "nie podano nazwiska";
        this.wiek =0;

    }
    public  Pracownik(String i , String n, int w){
        this.imie =i;
        this.nazwisko=n;
        this.wiek=w;

    }

    public  Pracownik(Pracownik prac){
        this.imie= prac.imie;
        this.nazwisko=prac.nazwisko;
        this.wiek=prac.wiek;
    }
}
