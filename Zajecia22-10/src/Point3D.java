public class Point3D {

    private int x;
    private int y;
    private int z;

    public Point3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Point3D(int newX, int newY, int newZ) {
        x = newX;
        y = newY;
        z = newZ;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public void setX(int newX) {
        x = newX;
    }

    public void setY(int newY) {
        y = newY;
    }

    public void setZ(int newY) {
        y = newY;
    }

    public void set(int newX, int newY, int newZ) {
        x = newX;
        y = newY;
        z = newZ;
    }

    public void capacity (){

    }

    public String toString() {
        String result = "{" + x + ", " + y + ", " + z + "}";
        return result;
    }

    public boolean equals(Point3D p) {
        boolean result;
        if (x == p.x && y == p.y && z == p.z) {
            result = true;
        }
        else {
            result = false;
        }
        return result;
    }

    public void movePoint(int mx, int my, int mz) {
        x = x + mx;
        y = y + my;
        z = z + mz;
    }

    public void clear() {
        x = 0;
        y = 0;
        z = 0;
    }

    public double distance(Point3D p) {
        double result = (p.x - x)*(p.x - x);
        result = result + (p.y - y)*(p.y - y);
        result = result + (p.z - z)*(p.z - z);
        result = Math.sqrt(result);
        return result;
    }

}
