import java.util.Scanner;
import java.util.Random;

public class Cube {

    private static Point3D point3D_1, point3D_2, point3D_3;

    private static String menu = "Program testujący punkty w 2D oraz w 3D.\n" +
            "1. Wprowadź 3 punkty w przestrzeni 3D.\n" +
            "2. Wylosuj 3 punkty w przestrzeni 3D.\n" +
            "3. Przesuń punkty w przestrzeni 3D o zadaną wartość.\n" +
            "4. Wyświetl stan programu (wszystkie punkty).\n" +
            "0. Zakończ działanie.\n" +
            "Co chcesz zrobić? ";

    private static Scanner cin;
    private static Random gen;

    private static void wprowadz3D() {
        int a, b, c;

        System.out.print("Wprowadź współrzędną X dla pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla pierwszego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla pierwszego punktu: ");
        c = cin.nextInt();
        point3D_1.set(a, b, c);

        System.out.print("Wprowadź współrzędną X dla drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla drugiego punktu: ");
        c = cin.nextInt();
        point3D_2.set(a, b, c);

        System.out.print("Wprowadź współrzędną X dla drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla drugiego punktu: ");
        c = cin.nextInt();
        point3D_3.set(a, b, c);
    }

    private static void wylosuj3D() {
        int a, b, c;

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        c = gen.nextInt(500);
        point3D_1.set(a, b, c);

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        c = gen.nextInt(500);
        point3D_2.set(a, b, c);

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        c = gen.nextInt(500);
        point3D_3.set(a, b, c);

        System.out.println("Punkty zostały pomyślnie wylosowane.");
    }

    private static void przesun3D() {
        int a, b, c;

        System.out.print("Wprowadź współrzędną X dla przesunięcia pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia pierwszego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla przesunięcia pierwszego punktu: ");
        c = cin.nextInt();
        point3D_1.movePoint(a, b, c);

        System.out.print("Wprowadź współrzędną X dla przesunięcia drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla przesunięcia drugiego punktu: ");
        c = cin.nextInt();
        point3D_2.movePoint(a, b, c);

        System.out.print("Wprowadź współrzędną X dla przesunięcia drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla przesunięcia drugiego punktu: ");
        c = cin.nextInt();
        point3D_3.movePoint(a, b, c);
    }

    private static void wyswietlStan() {
        System.out.println("------- STAN PROGRAMU -------");
        System.out.println("W przestrzeni 3D:");
        System.out.println("Punkt 1: " + point3D_1);
        System.out.println("Punkt 2: " + point3D_2);
        System.out.println("Punkt 2: " + point3D_3);
        System.out.println("-----------------------------");
    }

    public static void main(String[] args) {
        point3D_1 = new Point3D();
        point3D_2 = new Point3D();
        point3D_3 =new Point3D();

        cin = new Scanner(System.in);
        gen = new Random();

        int wybor;

        do {
            System.out.print(menu);
            wybor = cin.nextInt();

            switch(wybor) {
                case 1:
                    wprowadz3D();
                    break;
                case 2:
                    wylosuj3D();
                    break;
                case 3:
                    przesun3D();
                    break;
                case 4:
                    wyswietlStan();
                    break;
                case 0:
                    System.out.println("Do widzenia!");
                    break;
                default:
                    System.out.println("Niepoprawny wybór. Spróbuj jeszcze raz!");
            }

        } while (wybor != 0);
    }
}
