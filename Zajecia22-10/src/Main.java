import java.util.Scanner;
import java.util.Random;

public class Main {

    private static Point point1, point2;
    private static Point3D point3D_1, point3D_2;

    private static String menu = "Program testujący punkty w 2D oraz w 3D.\n" +
            "1. Wprowadź 2 punkty w przestrzeni 2D.\n" +
            "2. Wylosuj 2 punkty w przestrzeni 2D.\n" +
            "3. Przesuń punkty w przestrzeni 2D o zadaną wartość.\n" +
            "4. Wprowadź 2 punkty w przestrzeni 3D.\n" +
            "5. Wylosuj 2 punkty w przestrzeni 3D.\n" +
            "6. Przesuń punkty w przestrzeni 3D o zadaną wartość.\n" +
            "7. Wyświetl stan programu (wszystkie punkty).\n" +
            "0. Zakończ działanie.\n" +
            "Co chcesz zrobić? ";

    private static Scanner cin;
    private static Random gen;

    private static void wprowadz2D() {
        int a, b;

        System.out.print("Wprowadź współrzędną X dla pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla pierwszego punktu: ");
        b = cin.nextInt();
        point1.set(a, b);

        System.out.print("Wprowadź współrzędną X dla drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla drugiego punktu: ");
        b = cin.nextInt();
        point2.set(a, b);
    }

    private static void wylosuj2D() {
        int a, b;

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        point1.set(a, b);

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        point2.set(a, b);

        System.out.println("Punkty zostały pomyślnie wylosowane.");
    }

    private static void przesun2D() {
        int a, b;

        System.out.print("Wprowadź współrzędną X dla przesunięcia pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia pierwszego punktu: ");
        b = cin.nextInt();
        point1.movePoint(a, b);

        System.out.print("Wprowadź współrzędną X dla przesunięcia drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia drugiego punktu: ");
        b = cin.nextInt();
        point2.movePoint(a, b);
    }

    private static void wprowadz3D() {
        int a, b, c;

        System.out.print("Wprowadź współrzędną X dla pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla pierwszego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla pierwszego punktu: ");
        c = cin.nextInt();
        point3D_1.set(a, b, c);

        System.out.print("Wprowadź współrzędną X dla drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla drugiego punktu: ");
        c = cin.nextInt();
        point3D_2.set(a, b, c);
    }

    private static void wylosuj3D() {
        int a, b, c;

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        c = gen.nextInt(500);
        point3D_1.set(a, b, c);

        a = gen.nextInt(500);
        b = gen.nextInt(500);
        c = gen.nextInt(500);
        point3D_2.set(a, b, c);

        System.out.println("Punkty zostały pomyślnie wylosowane.");
    }

    private static void przesun3D() {
        int a, b, c;

        System.out.print("Wprowadź współrzędną X dla przesunięcia pierwszego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia pierwszego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla przesunięcia pierwszego punktu: ");
        c = cin.nextInt();
        point3D_1.movePoint(a, b, c);

        System.out.print("Wprowadź współrzędną X dla przesunięcia drugiego punktu: ");
        a = cin.nextInt();
        System.out.print("Wprowadź współrzędną Y dla przesunięcia drugiego punktu: ");
        b = cin.nextInt();
        System.out.print("Wprowadź współrzędną Z dla przesunięcia drugiego punktu: ");
        c = cin.nextInt();
        point3D_2.movePoint(a, b, c);
    }

    private static void wyswietlStan() {
        System.out.println("------- STAN PROGRAMU -------");
        System.out.println("W przestrzeni 2D:");
        System.out.println("Punkt 1: " + point1);
        System.out.println("Punkt 2: " + point2);
        System.out.println("Odległość między nimi: " + point1.distance(point2));
        System.out.println("-----------------------------");
        System.out.println("W przestrzeni 3D:");
        System.out.println("Punkt 1: " + point3D_1);
        System.out.println("Punkt 2: " + point3D_2);
        System.out.println("Odległość między nimi: " + point3D_1.distance(point3D_2));
        System.out.println("-----------------------------");
    }

    public static void main(String[] args) {
        point1 = new Point();
        point2 = new Point();
        point3D_1 = new Point3D();
        point3D_2 = new Point3D();

        cin = new Scanner(System.in);
        gen = new Random();

        int wybor;

        do {
            System.out.print(menu);
            wybor = cin.nextInt();

            switch(wybor) {
                case 1:
                    wprowadz2D();
                    break;
                case 2:
                    wylosuj2D();
                    break;
                case 3:
                    przesun2D();
                    break;
                case 4:
                    wprowadz3D();
                    break;
                case 5:
                    wylosuj3D();
                    break;
                case 6:
                    przesun3D();
                    break;
                case 7:
                    wyswietlStan();
                    break;
                case 0:
                    System.out.println("Do widzenia!");
                    break;
                default:
                    System.out.println("Niepoprawny wybór. Spróbuj jeszcze raz!");
            }

        } while (wybor != 0);
    }
}
