public class Zesp {
    public int re1, re2, im1, im2, reW, imW, suma;

    public Zesp() {
        re1 = 0;
        re2 = 0;
        im1 = 0;
        im2 = 0;
        suma = 0;
        reW = 0;
        imW = 0;
    }

    public Zesp(int re1, int re2, int im1, int im2) {
        this.re1 = re1;
        this.re2 = re2;
        this.re1 = im1;
        this.re2 = im2;
        this.reW = reW;
        this.imW = imW;

    }


    public void setIm1(int im1) {
        this.im1 = im1;
    }

    public void setIm2(int im2) {
        this.im2 = im2;
    }

    public void setRe1(int re1) {
        this.re1 = re1;
    }

    public void setRe2(int re2) {
        this.re2 = re2;
    }

    public int getRe2() {
        return re2;
    }

    public int getIm2() {
        return im2;
    }

    public int getRe1() {
        return re1;
    }

    public int getIm1() {
        return im1;
    }

    public String checkIm(int im1) {
        if (im1 < 0) {
            return ("Wprowadzono liczbę: " + re1 + " - " + Math.abs(im1) + "i");
        } else {
            return ("Wprowadzono liczbę: " + re1 + " + " + im1 + "i");
        }

    }

    public String checkIm2(int im2) {
        if (im2 < 0) {
            return ("Wprowadzono liczbę: " + re2 + " - " + Math.abs(im2) + "i");
        } else {
            return ("Wprowadzono liczbę: " + re2 + " + " + im2 + "i");
        }

    }

    public int sumRi() {
        reW = re1 + re2;
        imW = im1 + im2;
        int result = reW;
        return result;
    }

}
