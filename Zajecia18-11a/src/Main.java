import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Zwierze[] zwierzeta;

        System.out.print("Ile chcesz zwierzątek? ");
        int len = cin.nextInt();

        System.out.println("ile mają urosnąć?");
        int ile =cin.nextInt();

        zwierzeta = new Zwierze[len];
        for (int i = 0; i < len; i++) {
            System.out.print("Kot/Mysz/Krowa/Czlowiek? (1/2/3/4) ");
            int choice = cin.nextInt();

            if (choice == 1) {
                zwierzeta[i] = new Kot();
            }
            else if (choice == 2) {
                zwierzeta[i] = new Mysz();
            }
            else if (choice == 3) {
                zwierzeta[i] = new Krowa();
            }
            else if (choice == 4) {
                zwierzeta[i] = new Czlowiek();
            }
            else {
                zwierzeta[i] = null;
            }
        }

        System.out.println("Tak jedzą Twoje zwierzątka:");
        for (int i = 0; i < len; i++) {
            System.out.println("Zwierzątko " + i + ": " + zwierzeta[i].jedz());
        }

        System.out.println("A tak piszczą Twoje zwierzątka:");
        for (int i = 0; i < len; i++) {
            System.out.print("Zwierzątko " + i + ": ");
            zwierzeta[i].piszcz();
            System.out.println();
        }

        System.out.println("Tak rosną twoje zwierzątka");
        for (int i = 0; i <len ; i++) {
            System.out.print("Zwierzątko" + i + ":");
            zwierzeta[i].rosnij(ile);

        }
    }

}
