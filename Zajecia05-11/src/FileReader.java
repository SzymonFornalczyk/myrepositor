import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.print("Wprowadź nazwę pliku: ");
        Scanner cin = new Scanner(System.in);
        String filename = cin.nextLine();

        //String filename = "C:/a.txt";

        File file = new File(filename);
        Scanner fileIn = new Scanner(file);

        System.out.println("Zawartość pliku: ");
        while (fileIn.hasNextLine()) {
            System.out.println(fileIn.nextLine());
        }

    }
}
