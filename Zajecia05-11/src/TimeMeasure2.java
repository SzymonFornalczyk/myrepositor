import java.util.Locale;
import java.util.Scanner;

public class TimeMeasure2 {

    public static void main(String[] args) {
        long start = System.nanoTime();

        Scanner wejscie = new Scanner(System.in);
        wejscie.useLocale(Locale.US);   //zmiana systemu wprowadzania liczb dziesietnych - z kropka
        double liczba1, liczba2;

        System.out.print("Hej. Wprowadź pierwszą liczbę: ");
        liczba1 = wejscie.nextDouble();
        System.out.print("Teraz wprowadź drugą liczbę: ");
        liczba2 = wejscie.nextDouble();

        System.out.print("Co chcesz zrobić? (1-DOD, 2-ODJ, 3-MNO, 4-DZI) ");
        int dzialanie = wejscie.nextInt();

        switch (dzialanie) {
            case 1:
                System.out.println("" + liczba1 + " + " + liczba2 + " = " + (liczba1 + liczba2));
                break;
            case 2:
                System.out.println("" + liczba1 + " - " + liczba2 + " = " + (liczba1 - liczba2));
                break;
            case 3:
                System.out.println("" + liczba1 + " * " + liczba2 + " = " + (liczba1 * liczba2));
                break;
            case 4:
                System.out.println("" + liczba1 + " / " + liczba2 + " = " + (liczba1 / liczba2));
                break;
            default:
                System.out.println("Nie wybrałeś żadnego działania!");
                break;
        }

        long stop = System.nanoTime();
        System.out.println("Czas wykonania: " + (stop-start) + "nanosek.");

    }

}
