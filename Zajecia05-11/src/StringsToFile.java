import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 *
 * @author Java 5 Course by SDA
 * @verion 1.0
 */
public class StringsToFile {

    public static String[] strings = new String[10];

    public static void addString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                strings[i] = s;
                return;
            }
        }
    }

    public static void removeString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals(s)) {
                strings[i] = null;
                return;
            }
        }
    }

    public static void saveToFile(String filename) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(filename);
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                writer.println(strings[i]);
            }
        }
        writer.close();
    }

    public static void readFromFile(String filename) throws FileNotFoundException {
        File file = new File(filename);
        Scanner fileIn = new Scanner(file);

        int i = 0;
        while (fileIn.hasNextLine()) {
            strings[i] = fileIn.nextLine();
            i++;

            if (i == strings.length) return;
        }
    }

    public static void print() {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                System.out.println(strings[i]);
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner cin = new Scanner(System.in);
        String line;

        readFromFile("C:/a.txt");
        print();

        System.out.println();
        System.out.println("Teraz mozesz dodac nowe napisy: ");

        do {
            line = cin.nextLine();
            if (! line.equals("")) {
                addString(line);
            }
        } while (! line.equals(""));

        print();
        saveToFile("C:/a.txt");
    }

}
