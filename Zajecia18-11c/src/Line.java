public class Line extends Figure implements Movable {

    private Point start, end;

    private void sort() {
        if (start.getX() > end.getX() || (start.getX() == end.getX() && start.getY() > end.getY())) {
            Point temp = start;
            start = end;
            end = temp;
        }
    }

    public Line() {
        super(FigureType.LINE);
        start = new Point();
        end = new Point();
    }

    public Line(Point start, Point end) {
        super(FigureType.LINE);
        this.start = new Point(start);
        this.end = new Point(end);
        sort();
    }

    public Line(Line line) {
        this(line.start, line.end);
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    public void setStart(Point start) {
        this.start = start;
        sort();
    }

    public void setEnd(Point end) {
        this.end = end;
        sort();
    }

    @Override
    public double getArea() {
        return 0.0;
    }

    @Override
    public double getCircuit() {
        return 0.0;
    }

    @Override
    public Point[] getEdges() {
        Point[] result = new Point[2];
        result[0] = start;
        result[1] = end;
        return result;
    }

    @Override
    public void addEdge(Point p) {}

    @Override
    public void move(double x, double y) {
        start.move(x, y);
        end.move(x, y);
    }
}
