import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Random;

public class HomeWorkTable {

    public static Random generate = new Random();

    public static int sizeTable = 0;
    public static DecimalFormat df = new DecimalFormat("0.00");

    public static void exercise() {
        sizeTable = generate.nextInt(20);
        int[] table = new int[sizeTable];
        int searchNumber = 0;
        int count = 0;
        String in = JOptionPane.showInputDialog("podaj liczbe z przedzialu 0 - 19");
        searchNumber = Integer.parseInt(in);
        for (int index = 0; index < table.length; index++) {
            table[index] = generate.nextInt(20);
            if (table[index] == searchNumber)
                count++;
            System.out.print(table[index] + " ");
        }
        System.out.println();
        System.out.println("Liczba " + searchNumber + " pojawila się " + count);
    }

    public static void exercise2() {
        sizeTable = generate.nextInt(20);
        int[] table = new int[sizeTable];
        int result = 0;
        double arithmeticAvarege = 0;
        boolean changeLoop = false;
        int elementSmallerThanArithmeticAvarege = 0;
        int elementGreaterThanArithmeticAvarege = 0;

        for (int index = 0; index < table.length; index++) {
            table[index] = generate.nextInt(20);
            result += table[index];
            System.out.print(table[index] + ", ");
        }

        System.out.println();
        arithmeticAvarege = (double)result / sizeTable;

        for (int i = 0; i < table.length; i++) {
            if (table[i] > arithmeticAvarege)
                elementGreaterThanArithmeticAvarege++;
            else if (table[i] < arithmeticAvarege)
                elementSmallerThanArithmeticAvarege++;
        }

        System.out.println("srednia wynosi " + df.format(arithmeticAvarege) +
                " wiekszych liczb jest: " + elementGreaterThanArithmeticAvarege +
                " a mniejszych jest: " + elementSmallerThanArithmeticAvarege);
    }

    public static void exercise3() {
        int[] table = new int[20];
        int countOccurrences = 0;

        for (int i = 0; i < table.length; i++) {
            table[i] = generate.nextInt(10) + 1;
            System.out.print(table[i] + ", ");
        }
        System.out.println();

        for (int i = 1; i <= 10 ; i++) {                    //i - oznacza poszukiwaną wartość z przedziału <1, 10>
            int counter = 0;
            for (int j = 0; j < table.length; j++) {        //j - będzie indeksem tablicy, przejdzie po wszystkich jej elementach
                if (table[j] == i) {                        //jeżeli j-ty element jest równy poszukiwanemu (czyli i) to zwiększamy licznik (counter)
                    counter++;
                }
            }
            System.out.println("Ilość wystąpień " + i + ": " + counter);
        }
    }

    public static void main(String[] args) {
      //  exercise();
        //exercise2();
      exercise3();
    }
}
